#Install Docker Version 20.10.17 on Ubuntu 22.04
yes Y | sudo apt-get update
yes Y | sudo apt-get install ca-certificates
yes Y | sudo apt-get install curl
yes Y | sudo apt-get install gnupg
yes Y | sudo apt-get install lsb-release

sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

yes Y | sudo apt-get update

#List the available versions:
#apt-cache madison docker-ce | awk '{ print $3 }'

VERSION_STRING=5:20.10.17~3-0~ubuntu-jammy
yes Y | sudo apt-get install docker-ce=$VERSION_STRING docker-ce-cli=$VERSION_STRING containerd.io docker-compose-plugin

echo $(docker --version)
